rm(list=ls()) #Clean the environment
setwd("/Novosib") #You need to change the working directory
d<-readxl::read_xlsx("Новосибирск учащиеся06 2017update.xlsx",1, col_types = "text") #Need to change the filename

#Obtain variant/test form from user ID
d$variant<-substr(d$Username, 10, 10)

#Drop all vars beside observables and user id
#d1<- d[d$variant==1, c("Username", names(d)[38:137]) ]   #One variant
d1<- d[, c("Username", names(d)[c(38:137)])]  #Two variants

#Drop empty observables
d1<- d1[,names(d1)[colSums(is.na(d1))!=nrow(d1)]] 

#Give single names to observables
names(d1)<-as.character(reshape::colsplit(names(d1), " ", c("x", "y"))$y)


##Preparing table for net generation####

#Get the proficiency names from obersvables names
proficiencies<-names(d1)
proficiencies[substr(names(d1),1,2)=="Cr"]<-"Create"
proficiencies[substr(names(d1),1,2)=="CR"]<-"Create"
proficiencies[substr(names(d1),1,2)=="Cm"]<-"Communicate"
proficiencies[substr(names(d1),1,2)=="Co"]<-"Communicate"
proficiencies[substr(names(d1),1,1)=="A"]<-"Access"
proficiencies[substr(names(d1),1,1)=="D"]<-"Define"
proficiencies[substr(names(d1),1,1)=="E"]<-"Evaluate"
proficiencies[substr(names(d1),1,1)=="I"]<-"Integrate"
proficiencies[substr(names(d1),1,1)=="M"]<-"Manage"

#Create the clean table with obs, profs and diff&discr parameters#
table.for.net.generation<-
  data.frame(observables=names(d1),
            proficiencies=proficiencies,
            discrimination=rep(1, length(names(d1))),   #This is parameter that is fixed
            difficulty1=rep(-1, length(names(d1))),     #This is parameter that is fixed
            difficulty2=rep(0.5, length(names(d1))))    #This is parameter that is fixed
table.for.net.generation<-table.for.net.generation[table.for.net.generation$observables!="Username",]

#OR we can assign found parameters - at the momenet it's lousy - do not use it

#source("psychometrics.R") 

#table.for.net.generation[, c("discrimination", "difficulty1", "difficulty2")]<-NULL
#table.for.net.generation<-merge(table.for.net.generation, v2vars, by.x="observables", by.y="Row.names")
#table.for.net.generation<-table.for.net.generation[table.for.net.generation$discrimination>0,]
#table.for.net.generation<-table.for.net.generation[table.for.net.generation$discrimination>0,]
#table.for.net.generation<-table.for.net.generation[table.for.net.generation$difficulty2>table.for.net.generation$difficulty1,]
rm(proficiencies)


#Create the network

#http://pluto.coe.fsu.edu/RNetica/RNetica.html
#http://pluto.coe.fsu.edu/RNetica/RNetica_0.4-6.tgz
#One-time run
#install.packages("http://pluto.coe.fsu.edu/RNetica/RNetica_0.4-6.tgz", repos = NULL)
#install.packages("http://pluto.coe.fsu.edu/RNetica/PNetica_0.2-2.tgz", repos = NULL)
#Before using this you should install Netica API on your computer
library("PNetica")
####RNETICA PART#####
library("RNetica")
StopNetica()
StartNetica(license = "+RudnevM/NTF/310-5-OP/[ADD THE SECURITY CODE HERE]")

##### Generate network: ####
#It generates the network based on table.for.net.generation and saves it to generated.from.r.dne
source("generate.net.R")
generate.net(table.for.net.generation) 

source("evaluate.proficiencies.R")
source("q.evaluate.R")

##### Process cases: ####
#read network from file and quickly evaluate and save to d2
#Takes about 0.2 seconds for every case on Intel Core i7 machine

net<-ReadNetworks("generated.from.r.dne")
d2<-q.evaluate(net=net, 
               obs=names(d1)[2:101], 
               username=names(d1)[1], 
               data=d1)
#or
#d2<-q.evaluate(net, as.character(table.for.net.generation$observables), names(d1)[1], d1)

#Assign clear levels to ICL variable
d2$ICL<-factor(d2$ICL, levels=c(  "Developing",  "Below", "Basic", "Above",  "Advanced"))
table(d2$ICL)


#### Save the result ####
save(d2, file="evaluated_df.r")
#or
#xlsx::write.xlsx(d2, "evaluated.xlsx") 


