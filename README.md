# README #

This is the code that creates the Bayesian net using all the available observables in the data file and process cases predicting the levels of ICL in the students. The main reason for creating this project is to embed all the steps in the test scoring into the single environment of R and make it transparent for evaluation and critique.
Basically, these sripts get as an input a data file with observables and produces the same file with ICL and proficiency levels added to it. The next stage is to add the adiminstration-specific analysis part for the report to a specific client. It will also allow generation of automated reports.
Such an integrated approach is more reliable and transparent.

### What is it? ###

* ICL test  functions
* Version 0.2
* Uses Netica API

### Warnings ###

* Please comment every change in the code
* The code is raw, but commented with detail. It needs cleaning and more commenting and optimizing, intially written by me (not an expert developer)
* The problems of the net and the whole approach are mentioned in the comments
* The code should not be used forever, the whole thing should be turned completely to the R, this is possible with CPTtools and bnlearn packages. It means getting rid of commercial and buggy Netica API.